import { Component, OnInit } from '@angular/core';
import { FormBuilder , Validators, FormGroup} from '@angular/forms';
import { forbiddenNameValidator } from './shared/user-name.validators';
import { PasswordValidation } from './shared/password.validator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  registrationForm: FormGroup;

  get userName() {
    return this.registrationForm.get('userName');
  }

  get email() {
    return this.registrationForm.get('email');
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit(){
    this.registrationForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(3), forbiddenNameValidator(/password/)]],
     email: [''],
     subscribe: [false],
      password: [''],
      confirmPassword: [''],
  address: this.fb.group({
    city: [''],
    state: [''],
    postalCode: ['']
  })
    }, {validator: PasswordValidation});

    this.registrationForm.get('subscribe').valueChanges
    .subscribe(checkdValue => {
      const email = this.registrationForm.get('email');
      if(checkdValue) {
        email.setValidators(Validators.required);
      }else{
        email.clearValidators();
      }
      email.updateValueAndValidity();
    })
  }

  

//  registrationForm = new FormGroup({
//    userName: new FormControl('Johannes'),
//    password: new FormControl(''),
//    confirmPassword: new FormControl(''),
//    address: new FormGroup({
//      city: new FormControl(''),
//      state: new FormControl(''),
//      postalCode: new FormControl('')
//    })
//  });

 loadApiData() {
   this.registrationForm.patchValue({
     userName: 'Bruce',
     password: 'test',
     confirmPassword: 'test',
     address: {
       city: 'city',
       state: 'state',
       postalCode: '123456'
     }
   })
 }
}
